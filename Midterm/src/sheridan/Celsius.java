package sheridan;

/**
 *
 * @author Tamana Seddiqi - 991528861
 *
 */

public class Celsius {

	public static int fromFahrenheit(int value){
		return (int)Math.round(( value - 32 ) * (0.5556));
	}

}
