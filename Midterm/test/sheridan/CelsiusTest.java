package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;


/**
 *
 * @author Tamana Seddiqi - 991528861
 *
 */

public class CelsiusTest {


	@Test
	public void testFromFahrenheitRegular() {
		assertTrue("Invalid value", Celsius.fromFahrenheit(32) == 0 );
	}

	@Test
	public void testFromFahrenheitException() {
		assertFalse("Invalid value", Celsius.fromFahrenheit(0) == 0 );
	}


	@Test
	public void testFromFahrenheitBoundaryIn() {
		assertTrue("Invalid value", Celsius.fromFahrenheit(98) == 37);
	}

	@Test
	public void testFromFahrenheitBoundaryout() {
		assertFalse("Invalid value", Celsius.fromFahrenheit(100) == 37);
	}


//	@Test
//	public void testFromFahrenheitRegular() {
//		fail("Invalid value");
//	}
//
//	@Test
//	public void testFromFahrenheitException() {
//		fail("Invalid value");
//	}
//
//
//	@Test
//	public void testFromFahrenheitBoundaryIn() {
//		fail("Invalid value");
//	}
//
//	@Test
//	public void testFromFahrenheitBoundaryout() {
//		fail("Invalid value");
//	}







}
